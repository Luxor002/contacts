﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Contacts__.Resources;
using Microsoft.Phone.UserData;
using Microsoft.Phone.Tasks;
using System.Windows;
using System.Linq;

using System.Windows.Media.Imaging;
using Windows.Phone.PersonalInformation;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace Contacts__.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        CameraCaptureTask cameraCaptureTask;
        PhotoChooserTask photoChooserTask;
        Contacts Contacts;
        ContactStore store;

        public ObservableCollection<ItemViewModel> Items { get; private set; }
        public ObservableCollection<Contatto> Contatti { get; private set; }
        public ObservableCollection<AlphaKeyGroup<Contatto>> ContattiGrouped  { get; private set; }
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
            this.Contatti = new ObservableCollection<Contatto>();
            ContattiGrouped = new ObservableCollection<AlphaKeyGroup<Contatto>>();

            cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += new EventHandler<PhotoResult>( cameraCaptureTask_Completed);

            photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);

            //Caricamento dei contatti
            Contacts = new Contacts();
            Contacts.SearchCompleted += new EventHandler<ContactsSearchEventArgs>(Contatti_RicercaCompletata);

            // Codice di esempio per localizzare la ApplicationBar
            //BuildLocalizedApplicationBar();        

            this.initAsync();
        }

        private async void initAsync()
        {
            store = await ContactStore.CreateOrOpenAsync();
        }


        void Contatti_RicercaCompletata(object sender, ContactsSearchEventArgs e)
        {
            foreach (Contact c in e.Results)
            {
                //Se il contatto non stato creato con la nostra app, altrimenti lo prendiamo dallo store
                if (c.Accounts.ElementAt(0).Name != Utils.getOurContactAccountName())
                {
                    BitmapImage img = null;
                    if (c.GetPicture() != null)
                    {
                        img = new BitmapImage();
                        img.SetSource(c.GetPicture());
                    }

                    String tel = "", nome = "", cognome = "";
                    if (c.PhoneNumbers.Count() > 0)
                    {
                        tel = c.PhoneNumbers.ElementAtOrDefault(0).PhoneNumber.ToString();
                    }
                    if (c.CompleteName != null)
                    {
                        nome = c.CompleteName.FirstName;
                        cognome = c.CompleteName.LastName;
                    }
                    else
                    {
                        nome = c.DisplayName;
                    }
                    var nuovoContatto = new Contatto() { Nome = nome, Cognome = cognome, NumeroTelefono = tel, SavePhoto = false };
                    var stream = c.GetPicture();
                    if (stream != null) { 
                        var foto = new BitmapImage();
                        foto.SetSource(stream);
                        nuovoContatto.FotoBitmap = foto;
                    }
                    nuovoContatto.Gruppo = new string[] { nuovoContatto.Cognome != null ? nuovoContatto.Cognome[0].ToString() : nuovoContatto.Nome[0].ToString() };
                    this.Contatti.Add(nuovoContatto);
                }
            }

            ContattiGrouped = AlphaKeyGroup<Contatto>.CreateGroups(Contatti,
                System.Threading.Thread.CurrentThread.CurrentUICulture,
                (Contatto s) => { return s.Gruppo == null ? null : s.Gruppo[0]; }, true);
                          
        }

        /// <summary>
        /// Raccolta per oggetti ItemViewModel.
        /// </summary>

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Proprietà ViewModel di esempio: questa proprietà viene usata per visualizzare il relativo valore mediante un'associazione
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Proprietà di esempio che restituisce una stringa localizzata
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Crea e aggiunge alcuni oggetti ItemViewModel nella raccolta di elementi.
        /// </summary>
        public void LoadData()
        {
            // Dati di esempio; sostituirli con dati reali

            this.IsDataLoaded = true;
        }

        public async void refreshContatti()
        {
            store = await ContactStore.CreateOrOpenAsync();
            //Pulisco la lista di contatti
            this.Contatti.Clear();

            //Contatti di terzi
            Contacts.SearchAsync(String.Empty, FilterKind.None, "");

            //Nostri contatti
            ContactQueryResult result = store.CreateContactQuery();
            IReadOnlyList<StoredContact> contactList = await result.GetContactsAsync();

            foreach (StoredContact c in contactList)
            {
                Contatto cont = new Contatto();
                cont.ID = c.Id;
                cont.Cognome = c.FamilyName;
                cont.Nome = c.GivenName;
                var props = await c.GetPropertiesAsync();
                cont.Email = props.ContainsKey(KnownContactProperties.Email) ? props[KnownContactProperties.Email] + "" : null;
                cont.NumeroTelefono = props.ContainsKey(KnownContactProperties.MobileTelephone) ? props[KnownContactProperties.MobileTelephone] + "" : null;
                cont.Indirizzo = props.ContainsKey(KnownContactProperties.Address) ? ((Windows.Phone.PersonalInformation.ContactAddress)props[KnownContactProperties.Address]).StreetAddress + "" : null;
                cont.NickName = props.ContainsKey(KnownContactProperties.Nickname) ? props[KnownContactProperties.Nickname] + "" : null;
                cont.SavePhoto = true;
                //TODO finisci di impostare tutti i dati
                
                this.Contatti.Add(cont);
            }

            //MessageBox.Show("No contacts returned");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                MessageBox.Show(e.ChosenPhoto.Length.ToString());

                //Code to display the photo on the page in an image control named myImage.
                //System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();
                //bmp.SetSource(e.ChosenPhoto);
                //myImage.Source = bmp;
            }
        }

        void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                MessageBox.Show(e.ChosenPhoto.Length.ToString());

            }
        }
    }
}