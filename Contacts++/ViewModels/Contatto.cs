﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Xml.Serialization;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Contacts__.ViewModels
{
    public class Contatto : INotifyPropertyChanged
    {
        private string _ID = null;
        public string ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                BitmapImage imageToResave = FotoBitmap;
                string oldImgPath = FotoPath;
                this._ID = value;
                NotifyPropertyChanged("ID");
                if (imageToResave != null)
                {
                    FotoBitmap = imageToResave;
                    Utils.deletePhoto(oldImgPath);
                }
            }
        }

        private string _Nome;
        public string Nome
        {
            get
            {
                return _Nome;
            }
            set
            {
                if (value != _Nome && value != string.Empty)
                {
                    this._Nome = value;
                    NotifyPropertyChanged("Nome");
                }
            }
        }
        private string _Cognome;
        public string Cognome
        {
            get
            {
                return _Cognome;
            }
            set
            {
                if (value != _Cognome)
                {
                    this._Cognome = value;
                    NotifyPropertyChanged("Cognome");
                }
            }
        }
        private string _NumeroTelefono;
        public string NumeroTelefono
        {
            get
            {
                return _NumeroTelefono;
            }
            set
            {
                if (value != _NumeroTelefono && value != string.Empty)
                {
                    this._NumeroTelefono = value;
                    NotifyPropertyChanged("NumeroTelefono");
                }
            }
        }

        private string _Email;
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                if (value != _Email && value != string.Empty)
                {
                    this._Email = value;
                    NotifyPropertyChanged("Email");
                }
            }
        }
        
        private BitmapImage _FotoBitmap;
        public BitmapImage FotoBitmap
        {
            get
            {
                if (SavePhoto) { 
                    var stream = Utils.getPhotoStreamLocal(FotoPath);
                    if (stream == null)
                    {
                        _FotoBitmap = null;
                    }
                    else
                    {
                        var bitmap = new BitmapImage();
                        bitmap.SetSource(stream);
                        _FotoBitmap = bitmap;
                        stream.Close();
                        stream.Dispose();
                    }
                }
                return _FotoBitmap;
            }
            set
            {
                try
                {
                    if (SavePhoto)
                    {
                        Utils.savePhotoLocal(value, this.FotoPath);
                    }
                    _FotoBitmap = value;
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }
                NotifyPropertyChanged("FotoBitmap");
            }
        }
        
        public string NickName { get; set; }
        public string[] Gruppo { get; set; }
        public string Indirizzo { get; set; }
        public string SouneriaPath { get; set; }
        public bool SavePhoto { get; set; }
        
        public string FotoPath
        {
            get
            {
                if (this.ID == null)
                {
                    return "temp";
                }
                return this.ID.ToString().Replace("{", "").Replace("}", "");
            }
            private set
            {
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Stream getFotoStream()
        {
            return Utils.getPhotoStreamLocal(FotoPath);
        }
    }
}
