﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Contacts__.Resources;
using Microsoft.Phone.UserData;
using Microsoft.Phone.Tasks;
using Contacts__.ViewModels;
using System.Collections.ObjectModel;
using Microsoft.Phone.Globalization;
using System.Globalization;
using System.Threading.Tasks;

namespace Contacts__
{
    public partial class MainPage : PhoneApplicationPage
    {

        // Costruttore
        public MainPage()
        {
            InitializeComponent();

            // Impostare il contesto dei dati nel controllo casella di riepilogo su dati di esempio
            DataContext = App.ViewModel;


            // Codice di esempio per localizzare la ApplicationBar
            //BuildLocalizedApplicationBar();
      /*      List<Contatto> lista = new List<Contatto>();
            lista.Add(new Contatto() { Nome = "asda" , Cognome="wewewe", NumeroTelefono="1232131"});
            lista.Add(new Contatto() { Nome = "axcsadsda", Cognome = "sdawewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "asxczda", Cognome = "awewewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "acsda", Cognome = "vcxxcvwewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "asxczda", Cognome = "wbvbvcbvewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "axczsda", Cognome = "2332wewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "asxcda", Cognome = "aqwqeewewewe", NumeroTelefono = "1232131" });
            lista.Add(new Contatto() { Nome = "avsda", Cognome = "hgvcbwewewe", NumeroTelefono = "1232131" });
            List<AlphaKeyGroup<Contatto>> DataSource = AlphaKeyGroup<Contatto>.CreateGroups(lista,
                System.Threading.Thread.CurrentThread.CurrentUICulture,
                (Contatto s) => { return s.Cognome; }, true);
            lst_contasddsaatti.ItemsSource = DataSource;*/
        }

        void Contatti_RicercaCompletata(object sender, ContactsSearchEventArgs e)
        {
            
        }

        // Caricare i dati per gli elementi ViewModel
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var a = lst_contatti.ActualHeight;
            
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
            App.ViewModel.refreshContatti();
        }
     
        private void button_Click(object sender, RoutedEventArgs e)
        {
        }
        

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            //cameraCaptureTask.Show();
        }
      

        private void button_Copy1_Click(object sender, RoutedEventArgs e)
        {
        }

        private void button_Copy2_Click(object sender, RoutedEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = "2065550123";
            phoneCallTask.DisplayName = "Gage";

            phoneCallTask.Show();
        }

        private void gestoreMappe_Click(object sender, RoutedEventArgs e)
        {

        }

        public void sceltaFoto_Click(object sender, RoutedEventArgs e)
        {
      //      photoChooserTask.Show();
        }


        private void Pivot_Loaded(object sender, RoutedEventArgs e)
        {

        }

    

        private void AggiuntaConttato_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/addContacts.xaml", UriKind.Relative));
        }

      
        private void selezioneContatto(object sender, SelectionChangedEventArgs e)
        {
            Contatto contactSelezionato = ((LongListSelector)sender).SelectedItem as Contatto ;
            // ((LongListSelector)sender).SelectedItem
            //  Contatto contatto = e.

            //http://stackoverflow.com/questions/4953491/passing-data-from-page-to-page
            PhoneApplicationService.Current.State["contattoSelezionato"] = contactSelezionato;
            NavigationService.Navigate(new Uri("/ContactDetails.xaml", UriKind.Relative));
            //NavigationService.Navigate(new Uri("/ContactDetails.xaml?parameter=contactSelezionato", UriKind.Relative));

            //TODO il metodo qui sotto permette di passare direttamente un oggetto.
            //Non funziona perchè non abbiamo il target su win 8.1?
            //Frame.Navigate(typeof(ContactDetails), contactSelezionato);
        }

        private void lst_contasddsaatti_GotFocus(object sender, RoutedEventArgs e)
        {

            lst_contasddsaatti.ItemsSource = App.ViewModel.ContattiGrouped;
        }


        private async void PivotItem_Loaded(object sender, RoutedEventArgs e)
        {
            await Task.Delay(500);
            lst_contasddsaatti.ItemsSource = App.ViewModel.ContattiGrouped;

            // label1.Text = "";
        }


        //lst_contasddsaatti.ItemsSource = App.ViewModel.ContattiGrouped;


        // Codice di esempio per la realizzazione di una ApplicationBar localizzata
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Imposta la barra delle applicazioni della pagina su una nuova istanza di ApplicationBar
        //    ApplicationBar = nuova ApplicationBar();

        //    // Crea un nuovo pulsante e imposta il valore del testo sulla stringa localizzata da AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Crea una nuova voce di menu con la stringa localizzata da AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}



    }
}