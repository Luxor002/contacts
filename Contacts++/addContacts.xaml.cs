﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Windows.Phone.PersonalInformation;
using Contacts__.ViewModels;
using Microsoft.Phone.Tasks;
using System.IO;
using System.Windows.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Collections.Generic;

namespace Contacts__
{
    public partial class addContacts : PhoneApplicationPage
    {
        Contatto NuovoContatto;
        PhotoChooserTask photoChooserTask;
        CameraCaptureTask cameraCaptureTask;
        public addContacts()
        {
            InitializeComponent();
            photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
            cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += new EventHandler<PhotoResult>(cameraCaptureTask_Completed);

            NuovoContatto = new Contatto() { SavePhoto = true };
            ContentPanel.DataContext = NuovoContatto;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                Contatto contattoToEdit = null;
                try {
                    contattoToEdit = (Contatto)PhoneApplicationService.Current.State["contattoToEdit"];
                    PhoneApplicationService.Current.State["contattoToEdit"] = null; //Per sicurezza
                } catch (KeyNotFoundException)
                {
                    //Tutto ok, siamo in inserimento
                }

                if (contattoToEdit != null)
                {
                    //Modifica in corso di un contatto preesistente
                    NuovoContatto = contattoToEdit;
                } else
                {
                    //Inserimento di un nuovo contatto
                    NuovoContatto = new Contatto() { SavePhoto = true };
                }
                ContentPanel.DataContext = NuovoContatto;
            }
        }

        private void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                saveFoto(e.ChosenPhoto);
                var binding = img_Foto.GetBindingExpression(Image.SourceProperty);
                binding.UpdateSource();
            }
        }

        private void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK) { 
                saveFoto(e.ChosenPhoto);
                var binding = img_Foto.GetBindingExpression(Image.SourceProperty);
                binding.UpdateSource();
            }
        }

        private void saveFoto(Stream s)
        {
            var img = new BitmapImage();
            img.SetSource(s);
            NuovoContatto.FotoBitmap = img;
        }

        private void SalvaConttato_Click(object sender, EventArgs e)
        {
            Utils.updatePendingBindings();
            this.addPerson(NuovoContatto);
        }

        private async void addPerson(Contatto contatto)
        {
            try {
                var store = await ContactStore.CreateOrOpenAsync();
                
                if (contatto.Nome == null || contatto.Nome == "")
                {
                    showError("Inserire un nome per il contatto.");
                    return;
                }

                string Cognome = contatto.Cognome != null ? contatto.Cognome : "";
                string DisplayName = contatto.NickName;
                if (DisplayName == null || DisplayName == "")
                {
                    DisplayName = contatto.Nome + contatto.Cognome;
                }


                var contact = new StoredContact(store)
                {
                    DisplayName = DisplayName,
                    GivenName = contatto.Nome,
                    FamilyName = Cognome
                };

                using (var stream = contatto.getFotoStream()) { 
                    if (stream != null)
                        await contact.SetDisplayPictureAsync(stream.AsInputStream());
                }

                var props = await contact.GetPropertiesAsync();
                props.Add(KnownContactProperties.Email, contatto.Email);
                props.Add(KnownContactProperties.MobileTelephone, contatto.NumeroTelefono);
                props.Add(KnownContactProperties.Nickname, contatto.NickName);

                if (contatto.Indirizzo != null && contatto.Indirizzo != "")
                {
                    var indirizzo = new ContactAddress();
                    indirizzo.StreetAddress = contatto.Indirizzo;
                    props.Add(KnownContactProperties.Address, indirizzo);
                }

                if (contatto.ID != null)
                {
                    await contact.ReplaceExistingContactAsync(contatto.ID);
                }
                else
                {
                    await contact.SaveAsync();
                    contatto.ID = contact.Id;
                }

                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            } catch (ArgumentNullException ex)
            { 
                Console.WriteLine("Errore nel salvataggio: '{0}'", ex);
            }
        }

        private void showError(string text)
        {
            err_Nome.Text = "Campo obbligatorio";
        }

        private void galleryBtn_Click(object sender, RoutedEventArgs e)
        {
            photoChooserTask.Show();
        }

        private void cameraBtn_Click(object sender, RoutedEventArgs e)
        {
            cameraCaptureTask.Show();
        }
    }
}