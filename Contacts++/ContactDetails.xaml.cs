﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.UserData;
using Microsoft.Phone.Tasks;
using Contacts__.ViewModels;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Windows.Phone.PersonalInformation;

namespace Contacts__
{
    public partial class ContactDetails : PhoneApplicationPage
    {
        public Contatto ContattoSelezionato {get; set; }
        public string Nominativo { get; set; }

        public ContactDetails()
        {

          //  lbl_Nome.DataContext = ContattoSelezionato;
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {
                ContattoSelezionato = (Contatto)PhoneApplicationService.Current.State["contattoSelezionato"];
                PhoneApplicationService.Current.State["contattoSelezionato"] = null;
                ContentPanel.DataContext = ContattoSelezionato;
                Nominativo = ContattoSelezionato.Nome + " " + ContattoSelezionato.Cognome;
                Titolo.Text = Nominativo;
            }
            
            // lbl_Nome.Text = ContattoSelezionato.Nome +" "+ ContattoSelezionato.Cognome;
            //btn_chiama.Visibility = ContattoSelezionato.NumeroTelefono == null || ContattoSelezionato.NumeroTelefono == string.Empty ? Visibility.Collapsed : Visibility.Visible;
        }

        private void chiamaContatto_click(object sender, EventArgs e)
        {
            var tel = ContattoSelezionato.NumeroTelefono;

            if (tel != null && tel != "") {
                PhoneCallTask phoneCallTask = new PhoneCallTask();

                phoneCallTask.PhoneNumber = tel;
                phoneCallTask.DisplayName = ContattoSelezionato.Nome + " " + ContattoSelezionato.Cognome;

                phoneCallTask.Show();
            } else
            {
                MessageBox.Show("Il contatto non ha il numero di telefono", "Impossibile chiamare", MessageBoxButton.OK);
            }
        }

        private void mandaMail_click(object sender, EventArgs e)
        {
            var address = ContattoSelezionato.Email;

            if (address != null && address != "")
            {
                EmailComposeTask emailComposeTask = new EmailComposeTask();

                emailComposeTask.Subject = "";
                emailComposeTask.Body = "Corpo della mail..";
                emailComposeTask.To = address;

                emailComposeTask.Show();
            } else
            {
                MessageBox.Show("Il contatto non ha l'indirizzo email", "Impossibile creare mail", MessageBoxButton.OK);
            }
        }

        private void mandasms_click(object sender, EventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = ContattoSelezionato.NumeroTelefono;
            smsComposeTask.Body = "Corpo del messaggio..";

            smsComposeTask.Show();
        }

        public void editBtn_Click(object sender, EventArgs e)
        {
            PhoneApplicationService.Current.State["contattoToEdit"] = ContattoSelezionato;
            NavigationService.Navigate(new Uri("/addContacts.xaml", UriKind.Relative));
        }

        public async void deleteBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Cancellazione contatto", "Sei sicuro?", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                //ok -> delete contact
                var idToDelete = ContattoSelezionato.ID;

                var store = await ContactStore.CreateOrOpenAsync();
                await store.DeleteContactAsync(idToDelete);

                //And back to contact list
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            else
            {
                //cancel -> do nothing
                return;
            }
        }
    }
}