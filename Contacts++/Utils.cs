﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Contacts__
{
    class Utils
    {
        public static void updatePendingBindings()
        {
            object focusObj = FocusManager.GetFocusedElement();
            if (focusObj != null && focusObj is TextBox)
            {
                var binding = (focusObj as TextBox).GetBindingExpression(TextBox.TextProperty);
                binding.UpdateSource();
            }
        }

        /**
        * Restituisce il nome del nostro account (relativo ai contatti). Non ho trovato un modo per dinamicizzarlo. 
        */
        public static string getOurContactAccountName()
        {
            return "Contacts__";
        }

        public static string savePhotoLocal(BitmapImage photo, string photoName)
        {
            //string folderName = "profilePictures";
            var imageName = photoName + ".jpg";
            SaveImages(photo, imageName);
            return imageName;
        }

        private static void SaveImages(BitmapImage bitmap, string imageName)
        {
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (myIsolatedStorage.FileExists(imageName))
                {
                    myIsolatedStorage.DeleteFile(imageName);
                }
                if (bitmap != null)
                {
                    IsolatedStorageFileStream fileStream = myIsolatedStorage.CreateFile(imageName);
                    
                    WriteableBitmap wb = new WriteableBitmap(bitmap);

                    // Encode WriteableBitmap object to a JPEG stream.
                    //Extensions.SaveJpeg(wb, fileStream, wb.PixelWidth, wb.PixelHeight, 0, 85);

                    wb.SaveJpeg(fileStream, wb.PixelWidth, wb.PixelHeight, 0, 85);
                    fileStream.Close();
                    fileStream.Dispose();
                }
            }
        }

        public static Stream getPhotoStreamLocal(string photoname)
        {
            var filename = photoname + ".jpg";
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                try {
                    if (!myIsolatedStorage.FileExists(filename))
                    {
                        return null;
                    }
                    IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(filename, FileMode.Open, FileAccess.Read);
                    return fileStream;
                } catch (Exception ex)
                {
                    //Console.Write(ex);
                    return null;
                }
            }
        }

        public static void deletePhotoTemp()
        {
            string fotoName = "temp";
            deletePhoto(fotoName);
        }

        public static void deletePhoto(string fotoName)
        {
            SaveImages(null, fotoName + ".jpg");
        }
    }
}
